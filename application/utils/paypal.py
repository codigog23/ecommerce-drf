from requests import post
from django.conf import settings
from requests.auth import HTTPBasicAuth
from decimal import Decimal


EXCHANGE_AMOUNT = Decimal(3.70)


class PaypalClient:
    def __init__(self):
        self.url = 'https://api-m.sandbox.paypal.com'
        self.client_id = settings.PAYPAL_CLIENT_ID
        self.client_secret = settings.PAYPAL_CLIENT_SECRET
        self.access_token = None

        self.authentication()

        # https://developer.paypal.com/api/rest/authentication/
    def authentication(self):
        endpoint = f'{self.url}/v1/oauth2/token'
        basic_auth = HTTPBasicAuth(self.client_id, self.client_secret)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {'grant_type': 'client_credentials'}
        response = post(endpoint, auth=basic_auth, headers=headers, data=body)
        if response.status_code == 200:
            response_json = response.json()
            self.access_token = response_json['access_token']

    # https://developer.paypal.com/docs/api/orders/v2/
    def checkout_order(self, total_price):
        total_price_to_usd = round(total_price / EXCHANGE_AMOUNT, 2)

        endpoint = f'{self.url}/v2/checkout/orders'
        headers = {
            'Authorization': f'Bearer {self.access_token}',
            'Content-Type': 'application/json'
        }
        body = {
            'purchase_units': [
                {
                    'amount': {
                        'currency_code': 'USD',
                        'value': str(total_price_to_usd)
                    }
                }
            ],
            'intent': 'CAPTURE',
            'payment_source': {
                'paypal': {
                    'experience_context': {
                        'brand_name': 'DRF Ecommerce',
                        'landing_page': 'LOGIN',
                        'user_action': 'PAY_NOW',
                        'return_url': 'https://example.com/return_url',
                        'cancel_url': 'https://example.com/cancel_url'
                    }
                }
            }
        }
        response = post(endpoint, headers=headers, json=body)
        response_json = response.json()
        return {
            'id': response_json['id'],
            'status': response_json['status'],
            'url': response_json['links'][1]['href']
        }

    # https://developer.paypal.com/docs/api/orders/v2/#orders_capture
    def capture_order(self, paypal_id):
        endpoint = f'{self.url}/v2/checkout/orders/{paypal_id}/capture'
        headers = {
            'Authorization': f'Bearer {self.access_token}',
            'Content-Type': 'application/json'
        }
        response = post(endpoint, headers=headers)
        return response.json()
