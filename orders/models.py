from django.db import models
from users.models import UserModel
from products.models import ProductModel


# Create your models here.
class OrderModel(models.Model):
    # user_id
    user = models.ForeignKey(UserModel, on_delete=models.CASCADE)
    total_price = models.DecimalField(
        max_digits=5, decimal_places=2
    )  # 99999.99
    subtotal_price = models.DecimalField(max_digits=5, decimal_places=2)
    igv_price = models.DecimalField(max_digits=5, decimal_places=2)
    paypal_id = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=120, null=True, blank=True)
    payment_url = models.CharField(max_length=255, null=True, blank=True)
    create_date = models.DateTimeField(auto_now_add=True)  # auto_now: boolean

    class Meta:
        db_table = 'orders'


class OrderItemModel(models.Model):
    order = models.ForeignKey(OrderModel, on_delete=models.CASCADE)
    product = models.ForeignKey(ProductModel, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    quantity = models.IntegerField()

    class Meta:
        db_table = 'orders_items'
