from rest_framework import serializers
from rest_framework.exceptions import NotFound
from django.db import transaction
from shopping_carts.selectors import _fetch_all_items, _clean_items
from products.selectors import _reduce_stock_products
from products.models import ProductModel
from application.utils.paypal import PaypalClient
from .models import OrderModel, OrderItemModel


class OrderSerializer(serializers.Serializer):
    def save(self, user):
        user_id = user.id
        shopping_cart = _fetch_all_items(user_id)
        products = shopping_cart['products']
        prices = shopping_cart['prices']

        paypal = PaypalClient()

        # Validar los productos en el carrito de compras
        if not products:
            raise NotFound()

        with transaction.atomic():
            # Crear un pedido
            order = OrderModel.objects.create(
                user_id=user_id,
                total_price=prices['total'],
                subtotal_price=prices['sub_total'],
                igv_price=prices['igv']
            )

            # Detalle del pedido
            items = [
                OrderItemModel(
                    order_id=order.id,
                    product_id=product['id'],
                    price=product['price'],
                    quantity=product['quantity']
                )
                for product in products
            ]

            OrderItemModel.objects.bulk_create(items)

            # Obtener url de pago (PAYPAL)
            response_paypal = paypal.checkout_order(prices['total'])
            order.paypal_id = response_paypal['id']
            order.payment_url = response_paypal['url']
            order.status = response_paypal['status']
            order.save()

            # Reducir el stock de los productos
            reduces = _reduce_stock_products(products)
            ProductModel.objects.bulk_update(reduces, ['stock'])

            # Limpiar carrito de compras
            clean_shoping_cart = _clean_items(user_id)
            clean_shoping_cart.delete()

        return {
            'payment_url': response_paypal['url']
        }

    def capture(self, paypal_id):
        paypal = PaypalClient()
        response = paypal.capture_order(paypal_id)
        record = OrderModel.objects.filter(paypal_id=paypal_id).first()
        record.status = response['status']
        record.save()
