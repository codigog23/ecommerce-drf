from django.urls import path
from .views import OrderView, OrderWebhookView

urlpatterns = [
    path('', OrderView.as_view(), name='create'),
    path('webhook/', OrderWebhookView.as_view(), name='webhook')
]
