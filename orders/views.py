from rest_framework.viewsets import generics
from rest_framework.response import Response
from rest_framework import status, permissions
from .serializers import OrderSerializer


class OrderView(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['post']
    serializer_class = OrderSerializer

    def post(self, request):
        current_user = request.user
        serializer = self.serializer_class()
        serializer.save(current_user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class OrderWebhookView(generics.GenericAPIView):
    serializer_class = None

    def get_serializer(self):
        return None

    def post(self, request):
        body = request.data
        resource_id = body['resource']['id']
        print(f'Pedido -> {resource_id} ha llegado !')
        serializer = OrderSerializer()
        serializer.capture(resource_id)
        return Response(status=status.HTTP_200_OK)
