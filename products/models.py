from django.db import models


# Create your models here.
class ProductModel(models.Model):
    name = models.CharField(max_length=120)
    description = models.TextField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    stock = models.IntegerField(default=0)
    image = models.URLField()
    status = models.BooleanField(default=True)

    class Meta:
        db_table = 'products'
