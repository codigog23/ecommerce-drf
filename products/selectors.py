from .models import ProductModel


def _reduce_stock_products(products):
    updates = []

    for product in products:
        record = ProductModel.objects.filter(id=product['id']).first()
        new_stock = record.stock - product['quantity']
        record.stock = new_stock
        updates.append(record)

    return updates
