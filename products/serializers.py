from rest_framework import serializers
from .models import ProductModel
from application.utils.bucket import Bucket
from django.utils.text import slugify
from django.shortcuts import get_object_or_404

IMAGE_EXTENSION = '.png'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModel
        fields = '__all__'


class ProductCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120)
    description = serializers.CharField()
    price = serializers.DecimalField(max_digits=5, decimal_places=2)
    stock = serializers.IntegerField()
    image = serializers.ImageField(write_only=True)

    def create(self, validated_data):
        bucket = Bucket('drfecommerce', 'products')

        image = validated_data['image']
        name = validated_data['name']
        url = bucket.upload_object(
            f'{slugify(name)}{IMAGE_EXTENSION}', image.file
        )
        validated_data['image'] = url
        record = ProductModel(**validated_data)
        record.save()
        return record


class ProductUpdateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=120, required=False)
    description = serializers.CharField(required=False)
    price = serializers.DecimalField(
        max_digits=5, decimal_places=2, required=False)
    stock = serializers.IntegerField(required=False)
    image = serializers.ImageField(write_only=True, required=False)

    def update(self, instance, validated_data):
        image = validated_data.get('image')
        name = validated_data.get('name') or instance.name

        if image:
            bucket = Bucket('drfecommerce', 'products')
            url = bucket.upload_object(
                f'{slugify(name)}{IMAGE_EXTENSION}', image.file
            )
            validated_data['image'] = url

        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        return instance


class ProductDeleteSerializer(serializers.Serializer):
    id = serializers.IntegerField()

    def deactivate(self):
        product_id = self.validated_data['id']
        record = get_object_or_404(
            ProductModel, pk=product_id, status=True
        )
        record.status = False
        record.save()
