from django.urls import path
from .views import ProductView, ProductGetByIdView

urlpatterns = [
    path('', ProductView.as_view(), name='list_create'),
    path('<int:id>/', ProductGetByIdView.as_view(), name='read_update_delete')
]
