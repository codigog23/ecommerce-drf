from rest_framework.viewsets import generics
from rest_framework.response import Response
from rest_framework import status, parsers
from drf_yasg.utils import swagger_auto_schema
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import get_object_or_404
from .serializers import ProductSerializer, ProductCreateSerializer, ProductUpdateSerializer, ProductDeleteSerializer
from .models import ProductModel
from .schemas import ProductSchema


schema_request = ProductSchema()


class ProductView(generics.GenericAPIView):
    serializer_class = ProductSerializer
    http_method_names = ['get', 'post']
    queryset = ProductModel.objects
    parser_classes = [parsers.MultiPartParser]

    @swagger_auto_schema(
        operation_summary='Endpoint para listar los productos',
        operation_description='En este servicio se listara los productos',
        manual_parameters=schema_request.all()
    )
    def get(self, request):
        query_params = request.query_params
        nro_page = query_params.get('page')
        per_page = query_params.get('per_page')
        query = query_params.get('q', '')

        filters = {'status': True}

        records = self.queryset.filter(**filters).filter(
            Q(name__icontains=query) |
            Q(description__icontains=query)
        ).order_by('id')

        paginator = Paginator(records, per_page=per_page)
        page = paginator.get_page(nro_page)

        serializer = self.serializer_class(page.object_list, many=True)

        return Response({
            'results': serializer.data,
            'pagination': {
                'totalRecords': paginator.count,
                'totalPages': paginator.num_pages,
                'perPage': paginator.per_page,
                'currentPage': page.number
            }
        }, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary='Endpoint para crear un producto',
        operation_description='En este servicio se creara un producto',
        request_body=ProductCreateSerializer
    )
    def post(self, request):
        serializer = ProductCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProductGetByIdView(generics.GenericAPIView):
    serializer_class = ProductSerializer
    http_method_names = ['get', 'patch', 'delete']
    queryset = ProductModel.objects
    parser_classes = [parsers.MultiPartParser]

    @swagger_auto_schema()
    def get(self, request, id):
        record = get_object_or_404(
            self.queryset, pk=id, status=True
        )
        serializer = self.serializer_class(record, many=False)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=ProductUpdateSerializer
    )
    def patch(self, request, id):
        record = get_object_or_404(
            self.queryset, pk=id, status=True
        )
        serializer = ProductUpdateSerializer(record, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema()
    def delete(self, request, id):
        serializer = ProductDeleteSerializer(data={'id': id})
        serializer.is_valid(raise_exception=True)
        serializer.deactivate()
        return Response(status=status.HTTP_204_NO_CONTENT)
