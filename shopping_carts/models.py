from django.db import models
from users.models import UserModel
from products.models import ProductModel


# Create your models here.
class ShoppingCartModel(models.Model):
    product = models.ForeignKey(
        ProductModel, on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        UserModel, on_delete=models.CASCADE
    )
    quantity = models.IntegerField(default=1)

    class Meta:
        db_table = 'shopping_cart'
