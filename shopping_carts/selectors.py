from .models import ShoppingCartModel
from .serializers import ShoppingCartSerializer
from decimal import Decimal


def _fetch_all_items(user_id):
    records = ShoppingCartModel.objects.select_related(
        'product'
    ).filter(user_id=user_id).all()
    serializer = ShoppingCartSerializer(records, many=True)
    products = serializer.data
    prices = {
        'total': 0,
        'sub_total': 0,
        'igv': 0
    }

    if products:
        igv = Decimal(0.18)

        for product in products:
            price = Decimal(product['price'])
            quantity = product['quantity']
            product_price_total = round(price * quantity, 2)
            product['price_total'] = product_price_total
            prices['sub_total'] += product_price_total

        prices['igv'] = round(prices['sub_total'] * igv, 2)
        prices['total'] = round(prices['sub_total'] + prices['igv'], 2)

    return {
        'products': products,
        'prices': prices
    }


def _clean_items(user_id):
    return ShoppingCartModel.objects.filter(user_id=user_id)
