from rest_framework import serializers
from django.shortcuts import get_object_or_404
from .models import ShoppingCartModel


class ShoppingCartSerializer(serializers.Serializer):
    id = serializers.IntegerField(source='product.id')
    name = serializers.CharField(source='product.name')
    price = serializers.DecimalField(
        max_digits=5, decimal_places=2, source='product.price'
    )
    image = serializers.URLField(source='product.image')
    quantity = serializers.IntegerField()


class ShoppingCartUpdateSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()
    quantity = serializers.IntegerField(min_value=1)

    def update(self, instance, validated_data):
        product_id = validated_data['product_id']
        quantity = validated_data['quantity']
        user_id = instance.id

        validated_data['user_id'] = user_id

        # 1º Forma
        # record = ShoppingCartModel.objects.filter(
        #     user_id=user_id,
        #     product_id=product_id
        # ).first()

        # if record:
        #     # Actualizar la cantidad
        #     record.quantity = quantity
        # else:
        #     # Creamos el producto con el usuario
        #     record = ShoppingCartModel(**validated_data)

        # record.save()

        # 2º Forma
        ShoppingCartModel.objects.update_or_create(
            user_id=user_id,
            product_id=product_id,
            defaults={'quantity': quantity},
            create_defaults=validated_data
        )

        return validated_data


class ShoppingCartDeleteSerializer(serializers.Serializer):
    product_id = serializers.IntegerField()

    def remove(self, user_id):
        product_id = self.validated_data['product_id']
        record = get_object_or_404(
            ShoppingCartModel, user_id=user_id, product_id=product_id
        )
        record.delete()
