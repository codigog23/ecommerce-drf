from rest_framework.viewsets import generics
from rest_framework.response import Response
from rest_framework import status, permissions
from drf_yasg.utils import swagger_auto_schema
from .serializers import ShoppingCartSerializer, ShoppingCartUpdateSerializer, ShoppingCartDeleteSerializer
from .models import ShoppingCartModel
from .selectors import _fetch_all_items


# Create your views here.
# Listar los productos del carrito de compras (GET)
# Crear o Actualizar los productos del carrito (PUT)
class ShoppingCartView(generics.GenericAPIView):
    serializer_class = ShoppingCartSerializer
    queryset = ShoppingCartModel.objects
    http_method_names = ['get', 'put']
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        user_id = request.user.id
        records = _fetch_all_items(user_id)
        return Response(records, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=ShoppingCartUpdateSerializer
    )
    def put(self, request):
        serializer = ShoppingCartUpdateSerializer(
            request.user, data=request.data
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ShoppingCartDeleteView(generics.GenericAPIView):
    serializer_class = ShoppingCartDeleteSerializer
    http_method_names = ['delete']
    permission_classes = [permissions.IsAuthenticated]

    def delete(self, request, product_id):
        user_id = request.user.id
        serializer = self.serializer_class(data={'product_id': product_id})
        serializer.is_valid(raise_exception=True)
        serializer.remove(user_id)
        return Response(status=status.HTTP_204_NO_CONTENT)
